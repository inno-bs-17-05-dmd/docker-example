import Vue from "vue";
import Router from "vue-router";
import MainPage from "./views/MainPage.vue";
import queries from "./queries";

Vue.use(Router);

const routes = [
    {
        path: "/",
        name: "main-page",
        component: MainPage
    }
];

for (const query of queries) {
    routes.push({
        name: query.title,
        path: "/" + query.slug,
        component: query.component
    })
}

export default new Router({
    routes
});
