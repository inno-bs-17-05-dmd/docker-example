import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import "highlight.js/styles/github.css";
import VueHighlightJS from "vue-highlightjs";
import VueLayers from "vuelayers";
import 'vuelayers/lib/style.css'
import App from "./App.vue";
import router from "./router";

Vue.config.productionTip = false;
Vue.use(Vuetify);
Vue.use(VueHighlightJS);
Vue.use(VueLayers);

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
