import RegisterPatient from "./views/RegisterPatient";
import ListUsers from "./views/ListUsers";
import ListDoctors from "./views/ListDoctors";
import ListPrescriptions from "./views/ListPrescriptions";
import AvailableRoom from "./views/AvailableRoom";
import SpatialWorkingGeometry from "./views/SpatialWorkingGeometry";

export default [
    {
        title: "List all Users",
        slug: "list_users",
        component: ListUsers
    },
    {
        title: "List all Doctors",
        slug: "list_doctors",
        component: ListDoctors
    },
    {
        title: "List of appointments",
        slug: "list_appointments",
        component: ListPrescriptions
    },
    {
        title: "Check room availability",
        slug: "check_room",
        component: AvailableRoom
    },
    {
        title: "Register a Patient",
        slug: "register_patient",
        component: RegisterPatient
    },
    {
        title: "Spatial: Region search",
        slug: "spatial_working_geometry",
        component: SpatialWorkingGeometry
    }
];
