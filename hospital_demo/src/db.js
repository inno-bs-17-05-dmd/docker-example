import PouchDB from "pouchdb";
import geopouch from "geopouch";
import PouchDBFind from "pouchdb-find";

PouchDB.plugin(geopouch);
PouchDB.plugin(PouchDBFind);

export const users = PouchDB("hospital_users");
export const timeTables = PouchDB("time_tables");
export const appointments = PouchDB("hospital_appointments");
export const prescriptions = PouchDB("hospital_prescriptions");
export const medicines = PouchDB("hospital_medicines");
export const roomAllocations = PouchDB("hospital_room_allocations");

const baseURL = "http://dmd_project:VLdaeLk42eO9TZMIMB13LDECwvRE6dONwLnlVZRIp64lSJCdZO1BDTmZkjQTlwttxP4AzUDPQz3munOazyEwULm73Wiw57UBYDJ9icLaCHdbf9IX3VBYZBHU8IZw8Eki@ionagamed.ru:31338";
export const remoteUsers = PouchDB(baseURL + "/hospital_users");
export const remoteTimeTables = PouchDB(baseURL + "/hospital_time_tables");
export const remoteAppointments = PouchDB(baseURL + "/hospital_appointments");
export const remotePrescriptions = PouchDB(baseURL + "/hospital_prescriptions");
export const remoteMedicines = PouchDB(baseURL + "/hospital_medicines");
export const remoteRoomAllocations = PouchDB(baseURL + "/hospital_room_allocations");


users.sync(remoteUsers, {live: true, retry: true});
timeTables.sync(remoteTimeTables, {live: true, retry: true});
appointments.sync(remoteAppointments, {live: true, retry: true});
prescriptions.sync(remotePrescriptions, {live: true, retry: true});
medicines.sync(remoteMedicines, {live: true, retry: true});
roomAllocations.sync(remoteRoomAllocations, {live: true, retry: true});
