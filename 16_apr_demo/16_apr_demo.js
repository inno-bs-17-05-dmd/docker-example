const PouchDB = require("pouchdb");
const remotePassword = require("./secret");

function pprint(message) {
    console.log(JSON.stringify(message, null, 4));
}

(async function () {
    // create a local database
    const localRep2 = PouchDB("rep2");

    pprint(await localRep2.allDocs({include_docs: true}));

    // connect to a remote database
    const remoteRep2 = PouchDB(`http://dmd_project:${remotePassword}@ionagamed.ru:31338/rep2`);
    pprint(await remoteRep2.allDocs({include_docs: true}));

    // setup a live sync (which is analogous to a continuous replication)
    localRep2.sync(remoteRep2, {live: true, retry: true});

    // create a local document
    await localRep2.put({
        _id: "l.lygin",
        name: "Leonid",
        surname: "Lygin",
        counter: 1,
    });

    pprint(await remoteRep2.allDocs({include_docs: true}));
}) ();
