from flask import Flask
from cloudant.client import CouchDB


app = Flask('DMD II project')


db = CouchDB('dmd_project', 'dmd_project', url='http://couchdb:5984', connect=True)
print(db.all_dbs())


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
