DMD Project Schema Description
==============================

Team: BS-17-05-T3

Members:

* Leonid Lygin
* Denis Levkovets
* Artyom IUrin
* Elizaveta Kolchanova

Project: Hospital Management System (project 8)


Table of contents
=================

.. contents::
   :depth: 3


Base schema description
=======================

There are several entities which are used in the system, their descriptions are provided
in this document in this section, and their non-trivial use-cases are covered in the
next section.


Base schema
-----------

We have been presented with a basic schema, which will be written here
(all entity names are prefixed with "Base", to use RST hyperlinks properly):


Base Employee
~~~~~~~~~~~~~

Attributes:

* **EID** - employee ID, unique, primary key, ``sequence`` (sequential number)
* **salary** - employee salary, ``number``
* **E_address** - employee's address, ``string``
* **sex** - employee's sex, ``enum of {Male, Female}``
* **NID** - didn't get from the ER schema
* **E_name** - employee's name, ``multi-valued string``
* **history** - didn't get from the ER schema
* **contact_num** - phone number, ``multi-valued string``

Is a: `Base Receptionist`_, `Base Nurse`_, `Base Doctor`_


Base Receptionist
~~~~~~~~~~~~~~~~~

No additional attributes

Is a: `Base Employee`_

Relations:

* One Base Receptionist **maintains** one `Base Record`_


Base Record
~~~~~~~~~~~

Attributes:

* **appointment** - didn't get from the ER schema
* **patient_id** - ID of a `Base Patient`_
* **description** - description of an appointment, ``string``
* **record_no** - ID of this record, primary key, ``sequence`` (sequential number)


Base Doctor
~~~~~~~~~~~

No additional attributes

Is a: `Base Employee`_

Also is a: Base Trainee Doctor, Base Permanent Doctor, Base Visiting Doctor
(they do not have any additional attributes, so they are not present in the list)

Relations:

* One Base Doctor **attends** one `Base Patient`_


Base Patient
~~~~~~~~~~~~

Attributes:

* **PID** - patient's id, unique, primary key, ``sequence`` (sequential number)
* **name** - patient's name, ``string``
* **sex** - patient's sex, ``enum of {Male, Female}``
* **address** - patient's address, ``string``
* **Pdetails** - compound attribute, consists of:
    * **date admitted** - when did the patient begin to attend the hospital, ``timestamp``
    * **date discharged** - when did the patient stop attending the hospital, ``timestamp``
* **contact_no** - contact phone number, optional, ``multi-valued string``

Relations:

* One Base Patient gets **assigned** to one `Base Room`_
* One Base Patient gets **billed** for many Base Treatments with one `Base Medicine`_
* One Base Patient **attends** one `Base Doctor`_


Base Medicine
~~~~~~~~~~~~~

Attributes:

* **price** - price of this medicine, ``decimal``
* **quantity** - quantity of this medicine in stock, ``integer``
* **code** - unique code of the medicine, not a primary key for some reason, ``string``

Relations:

* One Base Medicine is **billed** from many `Base Patients <Base Patient>`_


Base Room
~~~~~~~~~

Attributes:

* **rooms_id** - room id, unique, primary key, ``sequential`` (sequential number)
* **room_type** - room type, ``multi-valued enum of {something domain-specific}``
* **period** - didn't get from the ER schema

Relations:

* One Base Room gets **assigned** for one `Base Patient`_


Base Nurse
~~~~~~~~~~

No additional attributes

Is a: `Base Employee`_

Relations:

* Many Base Nurses **govern** one `Base Room`_


Our modified schema
-------------------

We have come up with a few modifications, both to better support CouchDB's ideology,
and to add support for some missing functionality in the system


Modifications
~~~~~~~~~~~~~

* Add `Admin`_ entity
* Add `Pharmacist`_ entity
* Add `Accountant`_ entity
* Add `Laboratorist`_ entity
* Add `User`_ entity, with an IS A relation to all user entities

User
~~~~

Attributes:

* **id** - user id, primary key, ``string``
* **name** - user's name, ``string``
* **password** - user's password hash, ``string``
* **contact_no** - user's contact phone, ``multi-valued string``
* **email** - user's contact email, unique, ``string``
* **email_confirmation_hash** - unique hash for email verification (implementation
  detail), ``optional string``
* **email_recovery_hash** - unique hash for email recovery (implementation detail),
  ``optional string``
* **sms_recovery_pin** - unique PIN for SMS password recovery (imp. detail),
  ``optional string``
* **date_of_birth** - user's date of birth, ``date``
* **photo** - user's profile photo, ``image``
* **address** - user's address, ``compound attribute``, consists of:
  * **country** - ``string``
  * **zipcode** - ``string``
  * **city** - ``string``
  * **street** - ``string``
  * **building** - ``string``
  * **apartment** - ``string``

Is a: `Patient`_, `Employee`_


Employee
~~~~~~~~

No additional attributes

Is a: `User`_; `Doctor`_, `Nurse`_, `Receptionist`_, `Pharmacist`_, `Accountant`_,
`Laboratorist`_


Admin
~~~~~

We don't need the additional hassle for the admin, so we just keep to a bare minimum

Attributes:

* **id** - admin id, primary key, ``string``
* **password** - admin's password hash, ``string``


Receptionist
~~~~~~~~~~~~

No additional attributes

Is a: `Employee`_

Relations:

* One Receptionist **maintains** many `Records <Record>`_
* One Receptionist **manages** many `Time Tables <Time Table>`_


Accountant
~~~~~~~~~~

No additional attributes

Is a: `Employee`_


Pharmacist
~~~~~~~~~~

No additional attributes

Is a: `Employee`_


Patient
~~~~~~~

Attributes:

* **height** - patient's height, ``decimal``, in cm
* **weight** - patient's weight, ``decimal``, in kg

Is a: `User`_

Relations:

* Many Patients **get** many `Rooms <Room>`_ through `Room Allocation`_


Nurse
~~~~~

No additional attributes

Is a: `Employee`_

Relations:

* Many Nurses **govern** many `Rooms <Room>`_


Laboratorist
~~~~~~~~~~~~

No additional attributes

Is a: `Employee`_

Relations:

* Many Laboratorists **conduct** many `Tests <Test>`_


Doctor
~~~~~~

Attributes:

* **rating** - rating from 0 to 5, user-generated, ``float``

Is a: `Employee`_

Relations:

* One Doctor **assigns** many `Prescriptions <Prescription>`_
* One Doctor **has** many `Time Tables <Time Table>`_


Time Table
~~~~~~~~~~

Denormalized to better represent NoSQL

Attributes:

* **date** - date for which this time table is created, ``date``
* **working_hours_start_at** - when does the doctor start working, ``time``
* **working_hours_end_at** - when does the doctor stop working, ``time``
* **occupations** - ``array of Time Table Entries``, artificial objects with following
  attributes:
  * **start_at** - occupation's start time, ``time``
  * **end_at** - occupation's end time, ``time``
  * **description** - occupation's description, ``string``

May be worth noting that **occupations** does not include `Appointments <Appointment>`_;
during them the `Doctor`_ would automatically become occupied

Relations:

* Many Time Tables **are for** one `Doctor`_
* Many Time Tables **are managed by** one `Receptionist`_


Room
~~~~

Attributes:

* **id** - room's unique id, primary key, ``string``
* **type** - room's type, ``enum of {something domain-specific}``

Relations:

* Many Rooms are **governed** by many `Nurses <Nurse>`_
* Many Rooms are **allocated** to many `Patients <Patient>`_ through `Room Allocation`_


Room Allocation
~~~~~~~~~~~~~~~

Binding entity of one allocation of a room to a patient

Attributes:

* **id** - allocation's unique id, primary key, ``string``
* **start** - allocation's start datetime, ``timestamp``
* **end** - allocation's end datetime, ``optional timestamp``

End is optional, because the allocation may be ongoing

Relations: this is a "through" model for a many-to-many between `Patient`_ and `Room`_


Appointment
~~~~~~~~~~~

Attributes:

* **id** - appointment's unique id, primary key, ``string``
* **planned_time** - appointment's planned time, ``timestamp``

Is a: `Cancelled Appointment`_ or a `Confirmed Appointment`_

Relations:

* One Appointment **includes** one `Doctor`_
* One Appointment **includes** one `Patient`_


Cancelled Appointment
~~~~~~~~~~~~~~~~~~~~~

Attributes:

* **cancelled_at** - appointment's cancelled datetime, ``timestamp``

Is a: `Appointment`_


Confirmed Appointment
~~~~~~~~~~~~~~~~~~~~~

Attributes:

* **confirmed_at** - appointment's confirmed datetime, ``timestamp``

Is a: `Appointment`_


Started Appointment
~~~~~~~~~~~~~~~~~~~

Attributes:

* **started_at** - appointment's start datetime, ``timestamp``

Is a: `Confirmed Appointment`_


Finished Appointment
~~~~~~~~~~~~~~~~~~~~

Attributes:

* **finished_at** - appointment's finish datetime, ``timestamp``

Is a: `Started Appointment`_


Medicine
~~~~~~~~

Attributes:

* **code** - medicine's unique code, primary key, ``string``
* **price** - medicine's price, ``decimal``
* **in_stock** - how much of this medicine is in stock right now, ``integer``

Relations:

* Many Medicines are **contained** in many `Prescriptions <Prescription>`_
* Many Medicines are **requested** in many `Medicine Requests <Medicine Request>`_


Medicine Request
~~~~~~~~~~~~~~~~

An outgoing request for medicine

Attributes:

* **fulfilled** - whether this request has been fulfilled, ``boolean``

Relations:

* Many Medicine Requests **are for** many `Medicines <Medicine>`_


Appointment Report
~~~~~~~~~~~~~~~~~~

Attributes:

* **id** - report's unique id, ``string``
* **comments** - doctor's comments about the appointment, ``string``

Relations:

* One Appointment Report **is a result** of one `Started Appointment`_
* One Appointment Report **includes** many `Prescriptions <Prescription>`_


Prescription
~~~~~~~~~~~~

Attributes:

* **id** - prescription's unique id, primary key, ``string``
* **created_at** - when this was prescribed, ``timestamp``
* **fulfilled_at** - when this was fulfilled (sold to the patient), ``timestamp``

Prescription comments are supposed to be included in the `Appointment Report`_

Relations:

* One Prescription **contains** many `Medicines <Medicine>`_
* Many Prescriptions are **included** in one `Appointment Report`_


Test
~~~~

Attributes:

* **id** - test's unique id, primary key, ``string``
* **scheduled_at** - planned time, ``timestamp``
* **results** - test's results, ``optional string``

Is **results** is null, that means that test has not been yet conducted

Relations:

* Many Tests are **scheduled due to** one `Started Appointment`_
* Many Tests may be **conducted by** many `Laboratorists <Laboratorist>`_


Private Message Session
~~~~~~~~~~~~~~~~~~~~~~~

This is denormalized in the spirit of NoSQL design. Normalized version would be
to split messages into separate entities

Attributes:

* **messages** - ``array of Messages``, artificial objects with following attributes:
  * **sent_at** - message's sent datetime, ``timestamp``
  * **body** - message's body, ``string``
  * **sent_by** - who sent the message, ``enum of {Patient, Doctor}``

Relations:

* Many Private Message Sessions **are conducted by** one `Patient`_
* Many Private Message Sessions **are conducted by** one `Doctor`_
