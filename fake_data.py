import binascii
import datetime
import random
import hashlib

from cloudant import CouchDB
from dateutil.parser import parse
from faker import Faker
from faker.utils.text import slugify

fake = Faker()


def hash_password(password):
    return binascii.hexlify(hashlib.pbkdf2_hmac("sha256", password.encode("utf-8"), b"salt", 100000)).decode("utf-8")


def generate_random_kzn_region():
    min_lat = 55.703837
    min_long = 48.843522
    max_lat = 55.891691
    max_long = 49.364346

    p1 = [random.uniform(min_lat, max_lat), random.uniform(min_long, max_long)][::-1]
    p2 = [random.uniform(min_lat, max_lat), random.uniform(min_long, max_long)][::-1]
    p3 = [random.uniform(min_lat, max_lat), random.uniform(min_long, max_long)][::-1]

    return {
        "type": "Polygon",
        "coordinates": [
            [p1, p2, p3]
        ]
    }


def generate_user():
    user = {}

    if random.randint(0, 1) == 0:
        user["sex"] = "Female"
    else:
        user["sex"] = "Male"

    if user["sex"] == "Male":
        user["name"] = fake.name_male()
    else:
        user["name"] = fake.name_female()

    user["username"] = slugify(user["name"])
    user["_id"] = user["username"]
    user["password"] = hash_password("1234")
    user["contact_no"] = fake.phone_number()
    user["email"] = slugify(user["name"]) + "@" + fake.free_email_domain()
    user["date_of_birth"] = fake.date_of_birth(minimum_age=18).isoformat()
    user["address"] = {}
    user["address"]["country"] = fake.country()
    user["address"]["zipcode"] = fake.zipcode()
    user["address"]["city"] = fake.city()
    user["address"]["street"] = fake.street_name()
    user["address"]["building"] = fake.building_number()
    user["address"]["apartment"] = fake.numerify(text="###")

    return user


def generate_patient():
    patient = {
        "type": "patient",
        "user": generate_user(),
        "patient": {
            "height": random.randint(170, 190),
            "weight": random.randint(45, 90)
        }
    }
    patient["_id"] = patient["user"]["_id"]

    return patient


def generate_admin():
    admin = {
        "_id": "admin",
        "password": hash_password("adminpassword")
    }

    return admin


def generate_receptionist():
    receptionist = {
        "type": "receptionist",
        "user": generate_user()
    }
    receptionist["_id"] = receptionist["user"]["_id"]

    return receptionist


def generate_accountant():
    accountant = {
        "type": "accountant",
        "user": generate_user()
    }
    accountant["_id"] = accountant["user"]["_id"]

    return accountant


def generate_pharmacist():
    pharmacist = {
        "type": "pharmacist",
        "user": generate_user()
    }
    pharmacist["_id"] = pharmacist["user"]["_id"]

    return pharmacist


def generate_nurse():
    nurse = {
        "type": "nurse",
        "user": generate_user()
    }
    nurse["_id"] = nurse["user"]["_id"]

    return nurse


def generate_laboratorist():
    lab = {
        "type": "laboratorist",
        "user": generate_user()
    }
    lab["_id"] = lab["user"]["_id"]

    return lab


def generate_doctor():
    doctor = {
        "type": "doctor",
        "user": generate_user(),
        "doctor": {
            "rating": random.uniform(0, 5),
            "working_geometry": generate_random_kzn_region()
        }
    }
    doctor["_id"] = doctor["user"]["_id"]

    return doctor


def generate_time_table(doctor_id, receptionist_id):
    time_tables = []

    today = datetime.date.today()
    for i in range(7):
        day = today + datetime.timedelta(days=i)
        time_table = {
            "date": day.isoformat(),
            "working_hours_start_at": "09:00:00",
            "working_hours_end_at": "18:00:00",
            "occupations": []
        }
        predefined_occupations = [
            {
                "start_at": "09:00:00",
                "end_at": "10:00:00",
                "description": "cleaning the toilets"
            },
            {
                "start_at": "10:00:00",
                "end_at": "11:00:00",
                "description": "helping poor children"
            },
            {
                "start_at": "12:00:00",
                "end_at": "14:00:00",
                "description": "obtaining certification"
            },
        ]
        for occupation in predefined_occupations:
            if random.randint(0, 1) == 0:
                time_table["occupations"].append(occupation)

        time_table["managed_by"] = receptionist_id
        time_table["for"] = doctor_id

        time_table["_id"] = f"tt${doctor_id}${day.isoformat()}"

        time_tables.append(time_table)

    return time_tables


def generate_room(nurse_ids):
    room = {
        "_id": str(random.randint(100, 999)),
        "type": random.choice([
            "4-patient room",
            "6-patient room",
            "megavip super-room",
            "crappy MIPT dorm room"
        ]),
        "governed_by": nurse_ids
    }
    return room


def allocate_rooms(patients, rooms):
    allocations = []
    today = datetime.date.today()
    for patient in patients:
        patient_id = patient["_id"]
        room = random.choice(rooms)
        room_id = room["_id"]
        allocation = {
            "_id": f"ra${patient_id}${room_id}${today.isoformat()}",
            "start_at": datetime.datetime.now().isoformat(),
            "end_at": (datetime.datetime.now() + datetime.timedelta(days=3)).isoformat(),
            "patient_id": patient_id,
            "room_id": room_id
        }
        allocations.append(allocation)
    return allocations


def generate_appointment(patient_id, doctor_id):
    day = datetime.date(year=2019, month=4, day=9)
    confirmed_time = parse("2019-04-09 09:30")
    planned_time = parse("2019-04-09 10:00")
    start_time = parse("2019-04-09 10:00")
    end_time = parse("2019-04-09 10:10")

    def generate_simple_appointment():
        return {
            "_id": f"appointment${patient_id}${doctor_id}${day.isoformat()}",
            "planned_time": planned_time.isoformat()
        }

    def generate_cancelled_appointment():
        return {
            **generate_simple_appointment(),
            "cancelled_at": confirmed_time.isoformat()
        }

    def generate_confirmed_appointment():
        return {
            **generate_simple_appointment(),
            "confirmed_at": confirmed_time.isoformat()
        }

    def generate_started_appointment():
        return {
            **generate_confirmed_appointment(),
            "started_at": start_time.isoformat()
        }

    def generate_finished_appointment():
        return {
            **generate_started_appointment(),
            "finished_at": end_time.isoformat()
        }

    fn = random.choice([
        generate_simple_appointment,
        generate_cancelled_appointment,
        generate_confirmed_appointment,
        generate_started_appointment,
        generate_finished_appointment
    ])
    return fn()


def generate_medicine():
    medicine = {
        "_id": fake.ssn(),
        "price": random.randint(1, 5) * 100,
        "in_stock": random.randint(0, 100)
    }
    return medicine


def generate_medicine_request(medicine_id):
    request = {
        "_id": f"request${medicine_id}${datetime.date.today().isoformat()}",
        "fulfilled": random.randint(0, 1) == 0,
        "medicine_id": medicine_id
    }
    return request


def generate_appointment_report(appointment_id):
    report = {
        "_id": appointment_id,
        "comments": fake.paragraph()
    }
    return report


def generate_prescription(appointment_id, medicine_id):
    prescription = {
        "_id": f"{appointment_id}${medicine_id}",
        "created_at": datetime.datetime.now().isoformat(),
        "medicine_ids": [medicine_id],
        "appointment_id": appointment_id
    }
    return prescription


def generate_test(appointment_id, lab_ids):
    test = {
        "_id": f"{appointment_id}${datetime.datetime.now().isoformat()}",
        "results": fake.paragraph(),
        "conducted_by": lab_ids,
        "scheduled_at": datetime.datetime.now().isoformat(),
        "appointment_id": appointment_id
    }
    return test


def generate_message_session(patient_id, doctor_id):
    session = {
        "_id": f"{patient_id}${doctor_id}${datetime.datetime.now().isoformat()}",
        "patient_id": patient_id,
        "doctor_id": doctor_id,
        "messages": []
    }
    for i in range(10):
        session["messages"].append({
            "sent_at": (datetime.datetime.now() - datetime.timedelta(minutes=i)).isoformat(),
            "body": fake.sentence(),
            "sent_by": "Patient" if i % 2 == 0 else "Doctor"
        })
    return session


def main():
    mult = 10

    patients = []
    receptionists = []
    accountants = []
    pharmacists = []
    nurses = []
    laboratorists = []
    doctors = []
    for _ in range(mult):
        patients.append(generate_patient())
        receptionists.append(generate_receptionist())
        accountants.append(generate_accountant())
        pharmacists.append(generate_pharmacist())
        nurses.append(generate_nurse())
        laboratorists.append(generate_laboratorist())
        doctors.append(generate_doctor())
    users = [
        generate_admin(), *patients, *receptionists, *accountants, *pharmacists,
        *nurses, *laboratorists, *doctors
    ]

    time_tables = []
    for doctor, receptionist in zip(doctors, receptionists):
        time_tables.extend(generate_time_table(doctor["_id"], receptionist["_id"]))

    rooms = []
    for _ in range(mult):
        rooms.append(generate_room(random.sample(nurses, random.randint(mult // 3, 2 * mult // 3))))

    allocations = allocate_rooms(patients, rooms)

    appointments = []
    message_sessions = []
    for patient, doctor in zip(patients, doctors):
        appointments.append(generate_appointment(patient["_id"], doctor["_id"]))
        message_sessions.append(generate_message_session(patient["_id"], doctor["_id"]))

    medicines = []
    medicine_requests = []
    for _ in range(mult):
        medicine = generate_medicine()
        medicines.append(medicine)
        medicine_requests.append(generate_medicine_request(medicine["_id"]))

    appointment_reports = []
    for appointment in appointments:
        if "started_at" in appointment:
            appointment_reports.append(generate_appointment_report(appointment["_id"]))

    prescriptions = []
    for appointment, medicine in zip(appointments, medicines):
        prescriptions.append(generate_prescription(appointment["_id"], medicine["_id"]))

    tests = []
    for appointment in appointments:
        if "started_at" in appointment:
            tests.append(generate_test(appointment["_id"], random.sample(laboratorists, random.randint(mult // 3, 2 * mult // 3))))

    client = CouchDB(
        user="dmd_project",
        auth_token="VLdaeLk42eO9TZMIMB13LDECwvRE6dONwLnlVZRIp64lSJCdZO1BDTmZkjQTlwttxP4AzUDPQz3munOazyEwULm73Wiw57UBYDJ9icLaCHdbf9IX3VBYZBHU8IZw8Eki",
        url="http://ionagamed.ru:31338",
        connect=True
    )

    for db in client.all_dbs():
        client.delete_database(db)

    client.create_database("hospital_users")
    client["hospital_users"].bulk_docs(users)

    client.create_database("hospital_time_tables")
    client["hospital_time_tables"].bulk_docs(time_tables)

    client.create_database("hospital_rooms")
    client["hospital_rooms"].bulk_docs(rooms)

    client.create_database("hospital_room_allocations")
    client["hospital_room_allocations"].bulk_docs(allocations)

    client.create_database("hospital_appointments")
    client["hospital_appointments"].bulk_docs(appointments)

    client.create_database("hospital_message_sessions")
    client["hospital_message_sessions"].bulk_docs(message_sessions)

    client.create_database("hospital_medicines")
    client["hospital_medicines"].bulk_docs(medicines)

    client.create_database("hospital_medicine_requests")
    client["hospital_medicine_requests"].bulk_docs(medicine_requests)

    client.create_database("hospital_appointment_reports")
    client["hospital_appointment_reports"].bulk_docs(appointment_reports)

    client.create_database("hospital_prescriptions")
    client["hospital_prescriptions"].bulk_docs(prescriptions)

    client.create_database("hospital_tests")
    client["hospital_tests"].bulk_docs(tests)


if __name__ == '__main__':
    main()
